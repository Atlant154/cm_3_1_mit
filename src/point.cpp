#include "../include/point.h"

/*
 * Pointers to class objects are not returned,
 * the logical integrity of encapsulation is
 * not broken.
 * Constancy of values protects against
 * accidental rewriting of values. Rewriting
 * the values in this algorithm is not required.
*/


double point::X() const {
    return x_coordinate_;
}

double point::Y() const {
    return y_coordinate_;
}