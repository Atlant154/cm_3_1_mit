#include "../include/result.h"

#include <fstream>
#include <iostream>

void result::add(double x, double y) {
    results_.emplace_back(point(x, y));
}

std::vector<point> result::get_result_vector() const
{
    return results_;
}

void result::write_to_file(std::string filename) const {

    auto n = results_.size();

    std::fstream result_file;
    result_file.open("../result/" + filename + ".txt", std::ios::out | std::ios::trunc);

    for (auto iter = 0; iter < n; ++iter)
        result_file << results_[iter].X() << '\t' << results_[iter].Y() << '\n';

    result_file << std::endl;

    result_file.close();
}

void result::write_to_console(std::string filename) const {

    auto n = results_.size();

    std::cout << "Result console output. Info: " << filename << ".\n";

    for (auto iter = 0; iter < n; ++iter)
        std::cout << "X: " << results_[iter].X() << '\t' << "Y: " << results_[iter].Y() << '\n';

    std::cout << std::endl;
}

double result::last_x() const {
    return results_.back().X();
}

double result::last_y() const {
    return results_.back().Y();
}

double result::penultimate_y() const {
    return results_[results_.size() - 2].Y();
}