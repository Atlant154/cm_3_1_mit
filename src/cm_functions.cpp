#include "../include/cm_functions.h"

#include "../include/result.h"

#include <cmath>
#include <fstream>
#include <iostream>

double source_function(double x);

double source_function_derivative(double x);

double rh_function(double x);

void write_exact_solution(std::string filename);

void euler_explicit(unsigned h_num) {

    result euler_result;

    double h = (euler_result.x_right_bound_ - euler_result.x_left_bound_) / h_num;

    // Add boundary condition(U(0) = 0):
    euler_result.add(0.0, 0.0);

    //U_i = U_(i - 1) + f(X_(i - 1))
    for (auto iter = 1; iter <= h_num; ++iter)
        euler_result.add(euler_result.x_left_bound_ + iter * h,
                         euler_result.last_y() + h * (rh_function(euler_result.last_x()) - 2 * euler_result.last_y()));

    euler_result.write_to_file("euler_result_Splits:" + std::to_string(h_num));
    write_exact_solution("euler_result");

    /*
     * Console output:
     * euler_result.write_to_console();
     * Write to file:
     * euler_result.write_to_file("euler_result_" + std::to_string(h_num));
    */
}

void weight_scheme(unsigned h_num) {
    result weight_result;

    double h = (weight_result.x_right_bound_ - weight_result.x_left_bound_) / h_num;

    // Add boundary condition(U_0 = 0):
    weight_result.add(0.0, 0.0);

    // Add boundary condition(U_1 = )
    weight_result.add(h, h * source_function(weight_result.last_y()));


    double weight_coefficient = 0.2;

    for (auto iter = 2; iter <= h_num; ++iter) {
        weight_result.add(iter * h, (2 * weight_result.last_y() * (weight_coefficient + 2 * h - 1)
        - 2 * rh_function(weight_result.last_y() * h - weight_coefficient * weight_result.penultimate_y()))/(weight_coefficient - 2));
    }

    weight_result.write_to_file("weight_scheme_Split:" + std::to_string(h_num));
}

void write_exact_solution(std::string filename)
{
    result exact_solution;

    unsigned h_num = 10000;

    double h = (exact_solution.x_right_bound_ - exact_solution.x_left_bound_) / h_num;

    for(auto iter = 0; iter < h_num; ++iter)
        exact_solution.add(exact_solution.x_left_bound_ + iter * h, source_function(exact_solution.x_left_bound_ + iter * h));

    exact_solution.write_to_file(filename + "_Exact:");
}

double rh_function(double x)
{
    return 2 * x * cos(x) + cos(x) - x * sin(x);
}

double source_function(double x)
{
    return x * cos(x);
}