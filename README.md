# Solving ODEs: Euler explicit method and weight scheme

![logo](doc/logo.png)

## Table of content

- [Solving ODEs: Euler explicit method and weight scheme](#solving-odes--euler-explicit-method-and-weight-scheme)
  * [Requirements](#requirements)
  * [How to](#how-to)
  * [Visualization](#visualization)

## Requirements

1. [CMake](https://cmake.org/download/) v3.10 performance is guaranteed.
2. [G++](https://gcc.gnu.org/) v7.3.0 performance is guaranteed.
3. [Gnuplot](http://www.gnuplot.info/download.html) with configured PATH for visualization.

## How to

1. Clone repo: `git clone https://Atlant154@bitbucket.org/Atlant154/cm_3_1_mit.git`
2. Configure source function in `src/cm_functions.cpp -> rh_function()`
3. Configure number of greeting in `h_nums` vector in `main.cpp`
4. Build a graph of the approximate and exact solution: `cd result/ && sh g_visualization.sh`
5. Delete all results: `sh clear.sh`


## Visualization

For visualization it is used gnuplot with multiple output pf graphs on one window.  
To transfer the text files of the result in the gnuplot is used bash script. The script uses
environment variables and EOF, for this reason, i strongly recommend running this program on
Linux/OS X. If the script doesnt start correctly in your OS, you can always use its contents
for self-input into gnuplot.