#ifndef CM_3_1_MIT_POINT_H
#define CM_3_1_MIT_POINT_H

class point {
public:
    // Explicit class constructor(initializer list is used):
    explicit point(double x, double y) : x_coordinate_(x), y_coordinate_(y) {};

    // Default destructor:
    ~point() = default;

    // Getters:
    double X() const;
    double Y() const;

private:
    const double x_coordinate_;
    const double y_coordinate_;
};

#endif
