#ifndef CM_3_1_MIT_CM_FUNCTIONS_H
#define CM_3_1_MIT_CM_FUNCTIONS_H

#include <vector>

#include "point.h"

void euler_explicit(unsigned h_num);

void weight_scheme(unsigned h_num);

#endif
