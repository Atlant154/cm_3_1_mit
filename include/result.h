#ifndef CM_3_1_MIT_RESULT_H
#define CM_3_1_MIT_RESULT_H

#include <vector>
#include <string>

#include "point.h"

class result {
public:
    // Default constructor:
    explicit result() = default;
    // Default destructor:
    ~result() = default;
    // Getter for last X from vector:
    double last_x() const;
    // Getter for last Y from vector:
    double last_y() const;
    // Getter for penultimate Y from vector(weighing scheme):
    double penultimate_y() const;
    // Method of adding point to the end of the vector:
    void add(double x, double y);
    // Getter for result vector:
    std::vector<point> get_result_vector() const;
    // Method of writing the result vector to the file:
    void write_to_file(std::string filename) const;
    // Method of writing the result vector to the console:
    void write_to_console(std::string filename) const;

    // Constant boundary:
    const double x_left_bound_ = 0.0;
    const double x_right_bound_ = 1.0;
private:
    // Result vector consisting of points:
    std::vector<point> results_;
};

#endif