#include <iostream>
#include <vector>

#include "include/cm_functions.h"

int main() {
    std::cout << "Hello! This is a solution to laboratory work on computer methods: the third semester, the first work."
              << std::endl;

    std::vector<unsigned> h_nums = {2, 10, 20, 50, 100};

    for (auto iter = 0; iter < h_nums.size(); ++iter) {
        euler_explicit(h_nums.at(iter));
        weight_scheme(h_nums.at(iter));
    }
    return 0;
}